const cacheName = 'slitdecision-v2';
const contentToCache = [
    '/assets/gitlab-icon-rgb.svg',
    '/choices.css',
    '/index.html',
    '/main.css',
    '/main.js',
    '/favicon.ico',
];

self.addEventListener('install', (e) => {
    e.waitUntil((async () => {
        const cache = await caches.open(cacheName);
        await cache.addAll(contentToCache);
    })());
});

self.addEventListener('fetch', (e) => {
    e.respondWith((async () => {
        if (e.request.url.includes("qrng.anu.edu.au")) {
            return await fetch(e.request);
        } else {
            const r = await caches.match(e.request);
            if (r) { return r; }
            const response = await fetch(e.request);
            const cache = await caches.open(cacheName);
            cache.put(e.request, response.clone());
            return response;
        }
    })());
});

self.addEventListener('activate', (e) => {
    e.waitUntil(caches.keys().then((keyList) => {
        return Promise.all(keyList.map((key) => {
            if (key === cacheName) { return; }
            return caches.delete(key);
        }))
    }));
});
